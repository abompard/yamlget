YAML reader
===========

This repository contains a script to read a YAML file and print some values.
The aim is to make such a configuration file accessible to shell scripts.


Feel free to use it as you want (under the terms of the license). You can
contact me about it at <aurelien@bompard.org> or on my website:
<http://aurelien.bompard.org>.

Licensed under the GNU GPL version 3 or later.
(c) copyright Aurélien Bompard 2009-2014

